#+title: Devlogs
#+Author: Holychicken
#+Date:[2023-05-24 Wed]

* About
** The tui will help users look up hexdocs through terminal, unlike a terminal based browser it will have optmizations only for hexpm

* Progress
** <2023-05-25 Thu>
*** Started with repo and looking into zig
***  Got ncurses working with zig
**** Deep dive: using just commands to link zig with c libraries , add ~isystem~ flag or just use zig.build
