const std = @import("std");
const c = @cImport({
    @cInclude("ncurses.h");
});

pub fn main() !void {
    var e = c.initscr();
    e = c.printw("Hello World !!!");
    e = c.refresh();
    e = c.getch();
    e = c.endwin();
}
